package com.italosilva.lambda;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Implementacao {

	public void executaExpressaoLambda() {
		
		Calculator calc = (e, f) -> {
			e = e * f;
			System.out.println(e);
		};
		
		calc.calculate(5, 5);
		
		Calculator2 calc2 = e -> e * e;
		System.out.println(calc2.calculate(9));
		
	}
	
	public void executaCollections() {
		
		List<Integer> list = new ArrayList<>();
		list.add(4);
		list.add(5);
		list.add(2);
		list.add(1);
		list.add(0);
		list.add(3);
		
		list.sort((x,y) -> x.compareTo(y)); //Comparator
		list.replaceAll(e -> e*2); //UnaryOperator
		list.removeIf(e -> e % 2 != 0); //Predicate
		list.forEach(e -> System.out.println(e)); //Consumer (void)
		
	}
	
	public void executaReferenciarMetodos() {
		
		List<String> nomes = Arrays.asList("alfredo", "joana", "ricardo", "marina");
		
		/**
		 * 
		 * Entrada replace all - Unary Operator - Transformar dado para outro do mesmo tipo
		 * Metodos referencia devem ser static (pessoal)
		 * */ 
		nomes.replaceAll(String::toUpperCase);
		
		/**
		 * Referencia ao m�todo 
		 * Quando a express�o lambda tem entrada compat�vel com a do m�todo que ser� chamado
		 * Dado � uma String / sysout recebe String
		 * */
		nomes.forEach(System.out::println); 
		
	}
	
	public void executaClosure() {
		
		/**
		 * As variaveis locais usadas nas express�es Lambdas por default s�o final
		 * N�o � poss�vel mudar o valor ap�s sua declara��o 
		 * 
		 * */
		
		
	}
	
}
