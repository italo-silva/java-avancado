package com.italosilva.lambda;

@FunctionalInterface
public interface Calculator {
	
	void calculate(int x, int y);
}
