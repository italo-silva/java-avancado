package com.italosilva.lambda;

@FunctionalInterface
public interface Calculator2 {

	public int calculate(int x);
	
}
