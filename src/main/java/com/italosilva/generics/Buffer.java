package com.italosilva.generics;

import java.util.ArrayList;
import java.util.List;

public class Buffer<T> {
	
	private List<T> list = new ArrayList<T>();
	
	public void adicionar(T elemento) {
		list.add(elemento);
	}
	
	public T remover() {
		T elemento = list.get(0);
		list.remove(elemento);
		return elemento;
	}

}
