package com.italosilva.generics;

public class Implementacao {

	public void classGenericsExample () {
		
		Buffer<String> b = new Buffer<String>();
		b.adicionar("a");
		b.adicionar("b");
		b.adicionar("c");
		
		System.out.println(b.remover());
	}
}
