package com.italosilva.generics.wildcard;

import java.util.ArrayList;
import java.util.List;

public class Implementacao {
	
	public Implementacao() {
		
		List<Bebida> bebidas = new ArrayList<Bebida>();
		bebidas.add(new Cafe());
		bebidas.add(new Cha());
		Implementacao.prepararBebidas(bebidas);
		
		List<Cha> chas = new ArrayList<Cha>();
		chas.add(new Cha());
		Implementacao.prepararBebidas(chas);
		
		List<Cafe> cafes = new ArrayList<Cafe>();
		cafes.add(new Cafe());
		
		Implementacao.prepararBebidas(cafes);
		
		Implementacao.prepararBebidas2(chas);
		
		Implementacao.prepararBebidas3(chas);
		
	}
	
	public static void prepararBebidas(List<? extends Bebida> bebidas) {
		bebidas.forEach(b -> { //N�o � poss�vel adicionar
			b.preparar();
		});
	}
	
	public static void prepararBebidas2(List<? super Cha> bebidas) {
		
		bebidas.add(new Cha()); // � poss�vel adicionar
		
		bebidas.forEach(b -> { 
			((Bebida) b).preparar(); // Necess�rio tratar o tipo manual, que podem causar erros
		});
	}
	
	public static void prepararBebidas3 (List<?> bebidas) { // ? extends Object
		bebidas.forEach(b -> {
			((Bebida) b).preparar(); // Necess�rio tratar o tipo manual, que podem causar erros
		});
	}

}
