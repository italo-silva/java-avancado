package com.italosilva.reflection;

import java.io.File;
import java.io.FileNotFoundException;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.Scanner;

import com.italosilva.generics.wildcard.Bebida;

public class Implementacao {
	
	Class<Televisao> tvClass = Televisao.class;
	
	public void executaReflection () throws Exception{
		
		Field[] fields = tvClass.getDeclaredFields();
		
		for(Field f : fields) {
			System.out.println(f.getName()+" -> "+f.getType());
		}
		
		Method[] methods = tvClass.getDeclaredMethods();
		
		for(Method m : methods) {
			System.out.println(m.getName()+" -> "+Arrays.toString(m.getParameters())+" -> "+m.getReturnType());
		}
		
		Televisao tv1 = new Televisao();
		
		Method ligar = tvClass.getDeclaredMethod("ligar");
		ligar.invoke(tv1);
		
		Method mudarCanal = tvClass.getDeclaredMethod("mudarCanal", int.class);
		mudarCanal.invoke(tv1, 25);
		
	}
	
	public void executaReflectionNewInstance() throws Exception {
		String className;
		try (Scanner scanner = new Scanner(new File("class_name.txt"))){
			className = scanner.nextLine();
		}
		
		Class<Bebida> bebidaClass = (Class<Bebida>) Class.forName(className);
		Bebida bebida = bebidaClass.getDeclaredConstructor().newInstance();
		bebida.preparar();
	}

}
