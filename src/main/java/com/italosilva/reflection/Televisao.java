package com.italosilva.reflection;

public class Televisao {

	private boolean ligada;
	
	private int canal;

	public Televisao() {
		System.out.println("Televis�o criada");
	}
	
	public void ligar() {
		ligada = true;
		System.out.println("Ligada");		
	}
	
	public void desligar() {
		ligada = false;
		System.out.println("Desligada");
	}
	
	public void mudarCanal(int novoCanal) {
		canal = novoCanal;
		System.out.println("Novo canal "+ novoCanal);
	}
	
	public boolean isLigada() {
		return ligada;
	}
	
	public int getCanal() {
		return canal;
	}
	
}
