package com.italosilva.streamapi;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class Implementacao {
	
	private static List<String> itens = Arrays.asList("computador", "papel", "caneta", "quadro", "borracha", "cortina", "gaveta");
	
	public void executaBasicCollections() {
		
		/**
		 * ORD + LIMITA + IMPRIME
		 * */
		itens.stream()
			.sorted()
			.limit(3)
			.forEach(System.out::println);	
	}
	
	public void executaBasicCollections2() {
		
		/**
		 * FILTRA LETRA C + MONTA NOVA LISTA
		 * */
		
		List<String> itensFiltrados = itens.stream()
			.filter(e -> e.contains("c"))
			.collect(Collectors.toList());
		
		itensFiltrados.forEach(System.out::println);
	}
	
	public void executaBasicCollections3() {
		/**
		 * SOMA QTD DE CARCT DE ELEMENT + RETORNA SOMA
		 * */
		
		int count = itens.stream()
			.collect(Collectors.summingInt(e -> e.length()));
		
		System.out.println(count);
	}
	
	
	

}
